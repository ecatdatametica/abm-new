'use strict';

angular.module('demoAppApp')
    .controller('kpi2Controller', function ($scope, Principal,$mdDialog,DataSource,$rootScope) {
        Principal.identity().then(function(account) {
            var data_new = [
                { "Dist_lbl" : "*DIST", "Dist_Actdata":"11", "Dist_Utidata":"12" },
                { "SMV_lbl" : "SMV", "SMV_Actdata":"21", "SMV_Utidata":"22" },
                { "Prod_lbl" : "PROD", "Prodt_Actdata":"31", "Prod_Utidata":"32" },
                { "Product_lbl" : "PRODUCTION", "Product_Actdata":"41", "Product_Utidata":"42" },
                { "Uti_lbl" : "UTILIZATION", "Uti_Actdata":"51", "Uti_Utidata":"52" }
            ];

            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;



            var date='2015-05-26';

            $scope.HaulRdata=[];

            $scope.changeData=function(datelist,id){



                DataSource.kpiTwo(datelist).then(function (data) {
                    console.log(data,id)

                    $scope.mainData = [];
                    $scope.ExcaRdata={
                        labels: [],
                        series:  [
                            {
                                label: 'Productivity Achievement Excavator',
                                values: []
                            },
                            {
                                label: 'Productivity Achievement Hauler',
                                values: []
                            }
                            ,
                            {
                                label: 'Utilization Achievement Excavator',
                                values: []
                            },
                            {
                                label: 'Utilization Achievement Hauler',
                                values: []
                            }
                        ]
                    };
                    angular.copy(data, $scope.mainData)
                    $scope.rndBind=function(dataid){

                        angular.forEach($scope.mainData.rows,function(thisdatalist,key) {

                            if(thisdatalist[1]==dataid && thisdatalist[4]=='EXCAVATOR'){
                                $scope.ExcaRdata.labels.push(thisdatalist[1])
                                $scope.ExcaRdata.series[0].values.push(parseInt(thisdatalist[16]).toFixed(2))
                                $scope.ExcaRdata.series[2].values.push(parseInt(thisdatalist[26]).toFixed(2))

                               /* $scope.ExcaRdata.push(
                                [thisdatalist[16]]

                               // thisdatalist[26]


                                )*/
                            }

                           if(thisdatalist[1]==dataid && thisdatalist[3]=='HAULER'){
                                console.log(thisdatalist[15],thisdatalist[25],dataid)
                               $scope.ExcaRdata.series[1].values.push(parseInt(thisdatalist[15]).toFixed(2))
                               $scope.ExcaRdata.series[3].values.push(parseInt(thisdatalist[25]).toFixed(2))
                            }




                        });
                    }

                    $scope.rndBind('ABN-SNG')
                    $scope.rndBind('BOS-MLK')
                    $scope.rndBind('KJB-BRA')
                    $scope.rndBind('MFA-MBH')
                    $scope.rndBind('RJN-LJN')
                    $scope.rndBind('MHU-TJL')
                    $scope.rndBind('TIA-SDN')

                    console.log($scope.ExcaRdata)


                   $scope.finalDataRender($scope.ExcaRdata);

                });
            };


            $scope.changeData($rootScope.update,$rootScope.id)










            $scope.finalDataRender=function(data){

                var chartWidth       = 400,
                    barHeight        = 20,
                    groupHeight      = barHeight * data.series.length,
                    gapBetweenGroups = 10,
                    spaceForLabels   = 150,
                    spaceForLegend   = 150;

                // Zip the series data together (first values, second values, etc.)
                var zippedData = [];
                for (var i=0; i<data.labels.length; i++) {
                    for (var j=0; j<data.series.length; j++) {
                        zippedData.push(data.series[j].values[i]);
                    }
                }


                // Color scale
                var color = d3.scale.category20();
                var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;

                var x = d3.scale.linear()
                    .domain([0, d3.max(zippedData)])
                    .range([0, chartWidth]);

                var y = d3.scale.linear()
                    .range([chartHeight + gapBetweenGroups, 0]);

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .tickFormat('')
                    .tickSize(0)
                    .orient("left");

                // Specify the chart area and dimensions

                var chart = d3.select(".chart")
                    .attr("width", spaceForLabels + chartWidth + spaceForLegend)
                    .attr("height", chartHeight);

                // Create bars
                var bar = chart.selectAll("g")
                    .data(zippedData)
                    .enter().append("g")
                    .attr("transform", function(d, i) {
                        return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i/data.series.length))) + ")";
                    });

                // Create rectangles of the correct width
                bar.append("rect")
                    .attr("fill", function(d,i) { return color(i % data.series.length); })
                    .attr("class", "bar")
                    .attr("width", function(d) {
                        if(x(d)>=500){
                          console.log(x(d))
                            return (x(d))/4;
                        }else{
                            return (x(d)/4);
                        }

                    })
                    .attr("height", barHeight - 1);
               /* function(d) {
                    if(x(d)>=500){

                    }else{
                        return x(d);
                    }

                }*/
                // Add text label in bar
                bar.append("text")
                    .attr("x", function(d) {
                        if(x(d)>=500){
                            console.log(x(d))
                            return (x(d))/4;
                        }else{
                            return (x(d)/4);
                        }

                    })
                    .attr("y", barHeight / 2)
                    .attr("fill", "red")
                    .attr("dy", ".35em")
                    .text(function(d) { return d; });

                // Draw labels
                bar.append("text")
                    .attr("class", "label")
                    .attr("x", function(d) { return - 10; })
                    .attr("y", groupHeight / 2)
                    .attr("dy", ".35em")
                    .text(function(d,i) {
                        if (i % data.series.length === 0)
                            return data.labels[Math.floor(i/data.series.length)];
                        else
                            return ""});

                chart.append("g")
                    .attr("class", "y axis")
                    .attr("transform", "translate(" + spaceForLabels + ", " + -gapBetweenGroups/2 + ")")
                    .call(yAxis);

                // Draw legend
                var legendRectSize = 18,
                    legendSpacing  = 4;

                var legend = chart.selectAll('.legend')
                    .data(data.series)
                    .enter()
                    .append('g')
                    .attr('transform', function (d, i) {
                        var height = legendRectSize + legendSpacing;
                        var offset = -gapBetweenGroups/2;
                        var horz = spaceForLabels + chartWidth + 40 - legendRectSize;
                        var vert = i * height - offset;
                        return 'translate(' + horz + ',' + vert + ')';
                    });

                legend.append('rect')
                    .attr('width', legendRectSize)
                    .attr('height', legendRectSize)
                    .style('fill', function (d, i) { return color(i); })
                    .style('stroke', function (d, i) { return color(i); });

                legend.append('text')
                    .attr('class', 'legend')
                    .attr('x', legendRectSize + legendSpacing)
                    .attr('y', legendRectSize - legendSpacing)
                    .text(function (d) { return d.label; });

            }















        });
    });
function popcontrollerlist($scope, $mdDialog) {








    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
