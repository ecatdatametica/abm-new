'use strict';

angular.module('demoAppApp')
    .controller('kpi4Controller', function ($scope, Principal,DataSource) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            var date='2015-11-11';

            DataSource.kpiFour(date).then(function (data) {
                console.log(data);

                $scope.mainData = []
                angular.copy(data, $scope.mainData)

                $scope.tableData = []
                angular.forEach($scope.mainData.rows,function(thisdata) {

                    $scope.tableData.push({

                        'Name':thisdata[1],
                        'PrevDayActual':thisdata[2],
                        'TodayActual':thisdata[3],
                        'TodayTarget':thisdata[4],
                        'RevenueACT':thisdata[5],
                        'RevenueRFC':thisdata[6],
                        'RevenueDEV':thisdata[7],
                        'MTDActual':thisdata[8],
                        'MTDTarget':thisdata[9],
                        'Revenue2ACT':thisdata[10],
                        'Revenue2RFC':thisdata[11],
                        'Revenue2Actual':thisdata[12],
                        'StratMTD':thisdata[13],
                        'StratTarget':thisdata[14]
                    })

                });
            });
               /* $scope.datalist=[
                {
                    'name':'Sales Revenue',
                    'PrevDayActual':'111',
                    'TodayActual':'22',
                    'TodayTarget':'12',
                    'RevenueACT':'31',
                    'RevenueRFC':'23',
                    'RevenueDEV':' ',
                    'MTDActual':'32',
                    'MTDTarget':'45',
                    'Revenue2ACT':'31',
                    'Revenue2RFC':'32',
                    'Revenue2DEV':' ',
                    'StratMTD':'32',
                    'StratTarget':'48'
                },
                {
                    'name':'Cost of Sales',
                    'PrevDayActual':100
                }
            ]
*/
        });
    });