'use strict';
angular.module('demoAppApp')
    .controller('MainController', function ($scope, Principal,DataSource,$filter,$rootScope) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            $scope.sizes = [
                "ABN-SNG",
                "BOS-MLK",
                "KJB-BRA",
                "MFA-MBH",
                "RJN-LJN",
                "MHU-TJL",
                "TIA-SDN"

            ];
            $scope.sizes1 = [
                "MTD",
                "YTD"

            ];

            $scope.city = [
                "abc"


            ];
            $scope.state = [
                { category: 'meat', name: 'Jambi-TTG' }

            ];

            $scope.size="BOS-MLK"
            $scope.MTDdataMaster=[]

            $scope.changeData=function(id,date){

                DataSource.kpiOne(date).then(function (data) {
                    $rootScope.update=date
                    $rootScope.id=id
                    angular.copy(data,$scope.MTDdataMaster)
                    $scope.ExcaRdata=[]
                    $scope.HaulRdata=[]
                    $scope.MasterChartData=[]
                    angular.forEach($scope.MTDdataMaster.rows,function(thisdatalist,key) {

                        if(thisdatalist[1]==id && thisdatalist[4]=='EXCAVATOR'){

                            $scope.ExcaRdata.push({
                                'DIST':parseInt(thisdatalist[6]),
                                'SMU':parseInt(thisdatalist[8]),
                                'PROD':parseInt(thisdatalist[10]),
                                'PROD_ACTUAL_EXCAVATOR':parseInt(thisdatalist[12]),
                                'PROD_TARGET_EXCAVATOR':parseInt(thisdatalist[14]),
                                'PROD_ACH_EXCAVATOR':parseInt(thisdatalist[16]),
                                'UTL_ACTUAL_EXCAVATOR':parseInt(thisdatalist[22]),
                                'UTL_TARGET_EXCAVATOR':parseInt(thisdatalist[24]),
                                'UTL_ACH_EXCAVATOR':parseInt(thisdatalist[26])


                            })
                        }

                        if(thisdatalist[1]==id && thisdatalist[3]=='HAULER'){

                            $scope.HaulRdata.push({
                                'DIST':parseInt(thisdatalist[5]),
                                'SMU':parseInt(thisdatalist[7]),
                                'PROD':parseInt(thisdatalist[9]),
                                'PROD_ACTUAL_HAULER':parseInt(thisdatalist[11]),
                                'PROD_TARGET_HAULER':parseInt(thisdatalist[13]),
                                'PROD_ACH_HAULER':parseInt(thisdatalist[15]),
                                'UTL_ACTUAL_HAULER':parseInt(thisdatalist[21]),
                                'UTL_TARGET_HAULER':parseInt(thisdatalist[23]),
                                'UTL_ACH_HAULER':parseInt(thisdatalist[25])


                            })
                        }




                    });
                    $scope.rndBind=function(dataid){
                        angular.forEach($scope.MTDdataMaster.rows,function(thisdatalist,key) {
                            if(thisdatalist[1]==id && thisdatalist[4]==dataid ){
                                console.log(thisdatalist[10])
                                console.log(id,dataid)
                                $scope.MasterChartData.push({
                                    'State':dataid,
                                    'freq':{high:thisdatalist[6]},
                                    'PROD_ACTUAL_EXCAVATOR':thisdatalist[12],
                                    'PROD_TARGET_EXCAVATOR':thisdatalist[14],
                                    'PROD_ACH_EXCAVATOR':thisdatalist[16],
                                    'UTL_ACTUAL_EXCAVATOR':thisdatalist[22],
                                    'UTL_TARGET_EXCAVATOR':thisdatalist[24],
                                    'SMU_EXCAVATOR':thisdatalist[8],
                                    'PROD_EXCAVATOR':thisdatalist[10],
                                    'UTL_ACH_EXCAVATOR':thisdatalist[26],
                                    'prod_ACH_EXCAVATOR':parseInt(thisdatalist[16])


                                })
                            }
                            if(thisdatalist[1]==id && thisdatalist[3]==dataid ){

                                $scope.MasterChartData.push({
                                    'State':dataid,
                                    'freq':{high:thisdatalist[5]},
                                    'PROD_ACTUAL_EXCAVATOR':thisdatalist[11],
                                    'PROD_TARGET_EXCAVATOR':thisdatalist[13],
                                    'PROD_ACH_EXCAVATOR':thisdatalist[15],
                                    'UTL_ACTUAL_EXCAVATOR':thisdatalist[21],
                                    'UTL_TARGET_EXCAVATOR':thisdatalist[23],
                                    'SMU_EXCAVATOR':thisdatalist[7],
                                    'PROD_EXCAVATOR':thisdatalist[9],
                                    'UTL_ACH_EXCAVATOR':thisdatalist[25],
                                    'prod_ACH_EXCAVATOR':parseInt(thisdatalist[15])

                                })
                            }
                        });
                    }
                    /* Excavator 350t
                     Excavator 250t
                     Excavator 100t
                     Excavator 90t
                     Excavator 45t
                     OHT 100t
                     OHT 70t
                     OHT 60t
                     ADT 40t*/

                    $scope.rndBind('Excavator 350t')
                    $scope.rndBind('Excavator 250t')
                    $scope.rndBind('Excavator 100t')
                    $scope.rndBind('Excavator 45t')
                    $scope.rndBind('Excavator 90t')
                    $scope.rndBind('OHT 100t')
                    $scope.rndBind('OHT 70t')
                    $scope.rndBind('OHT 60t')
                    $scope.rndBind('ADT 40t')
                    console.log($scope.MasterChartData[0].PROD_ACTUAL_EXCAVATOR)

                    var freqData=[
                        {State:'AL',freq:{ high:249}}
                        ,{State:'AZ',freq:{ high:674}}
                        ,{State:'CT',freq:{ high:418}}

                    ];
                    $scope.chartOneData={'data1':400,'data2':4000};

                    $scope.dashboard($scope.MasterChartData);
                    console.log($scope.chartOneData.data1)
                    var datah=$filter('number')($scope.MasterChartData[0].PROD_ACTUAL_EXCAVATOR , 2);
                    var datah1=$filter('number')($scope.MasterChartData[0].PROD_TARGET_EXCAVATOR , 2);

                    $scope.datapro=$filter('number')($scope.MasterChartData[0].UTL_ACTUAL_EXCAVATOR , 2);
                    $scope.datapro1=$filter('number')($scope.MasterChartData[0].UTL_TARGET_EXCAVATOR , 2);
                    $scope.datapro2=$filter('number')($scope.MasterChartData[0].UTL_ACH_EXCAVATOR , 2);
                    $scope.datapro3=$filter('number')($scope.MasterChartData[0].prod_ACH_EXCAVATOR , 2);
                    $scope.chartOne(datah)
                    $scope.chartOne1(datah1)
                    $scope.fiunctionone($scope.datapro,$scope.datapro1,$scope.datapro2,$scope.datapro3)

                    var smudata=parseInt($filter('number')($scope.MasterChartData[0].SMU_EXCAVATOR , 2));

                    $scope.renddata(smudata)
                    // animate(/*$('#inputVal').val()*/.75);
                    var smudatalast=$scope.MasterChartData[0].PROD_EXCAVATOR ;
                    console.log( $scope.MasterChartData)
                    console.log(typeof smudatalast)
                    $scope.chartSndR(smudatalast);
                });
            }
            var dataSelectother='2015-09-29'
            $scope.changeData($scope.size,dataSelectother)

            $scope.changeDateList=function(){


                if( typeof $scope.user !='undefined'){
                    var dataSelect=$filter('date')($scope.user.submissionDate, "yyyy-MM-dd");
                    console.log('fst')
                    console.log(dataSelect)
                    console.log($scope.size)
                    $scope.changeData($scope.size,dataSelect)
                }else{
                    console.log('snd')
                    console.log(dataSelectother)
                    console.log($scope.size)
                    $scope.changeData($scope.size,dataSelectother)
                }



            }
            $scope.fiunctionone=function(data1,data2,data3,data4){
var dat=data3+' %'
var dat1=data4+' %'
var dat4=data1+' hr'
var dat5=data2+' hr'
                angular.element('#dataadd1').text(dat4)
                angular.element('#dataadd2').text(dat5)
                angular.element('#dataadd3').text(dat)
                angular.element('#dataadd4').text(dat1)

                $(".progress-bar").animate({width: data1 }, 100);
                $(".progone").animate({width: data2 }, 100);
            }

            var chartmain = c3.generate({
                bindto: '#container1',
                data: {
                    columns: [
                        ['data', 0]
                    ],
                    type: 'gauge'
                },
                gauge: {
                    label: {
                        format: function(value, ratio) {
                            return value;
                        }
                    },
                    min: 0,
                    max: 1800
                },
                size: {
                    width: 105,
                    height: 105
                }
            });
            var chartmain2 = c3.generate({
                bindto: '#container2',
                data: {
                    columns: [
                        ['data', 0]
                    ],
                    type: 'gauge'
                },
                gauge: {
                    label: {
                        format: function(value, ratio) {
                            return value;
                        }
                    },
                    min: 0,
                    max: 1800
                },
                size: {
                    width: 105,
                    height: 105
                },
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
//            unit: 'value', // percentage is default
//            max: 200, // 100 is default
                        values: [30, 60, 90, 100]
                    }
                }
            });
            $scope.startvalue=40
            $scope.chartOne=function(data1){


                chartmain.load({
                    columns: [[ 'Target',data1 ]]
                });
                d3.select("body #container1 .c3-target-data").remove()

            }
            $scope.chartOne1=function(data1){



                chartmain2.load({
                    columns: [[ 'Target',data1]]
                });
                d3.select("body #container1 .c3-target-data").remove()


            }
            $scope.renddata=function(data){

                $(function () {
                    var temperature = new Highcharts.Chart({
                        chart: {
                            renderTo: 'vucontainer',
                            type: 'gauge',
                            backgroundColor: '#f1f1f1'

                        },
                        title: {
                            text: 'SMU'
                            /*text: '',
                             style: {
                             display: 'none'
                             }*/
                        },
                        pane: [{
                            startAngle: -45,
                            endAngle: 45,
                            background: null,
                            center: ['50%', '140%'],
                            size: 200

                        }],
                        credits: {
                            enabled: false
                        },
                        yAxis: [{
                            min: 0,
                            max: 300,
                            minorTickPosition: 'outside',
                            tickPosition: 'outside',
                            tickInterval: 50,
                            labels: {
                                rotation: 'auto',
                                distance: 20
                            },
                            plotBands: [{
                                from: 25,
                                to: 40,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },
                                {
                                    from: -40,
                                    to: -10,
                                    color: '#0000FF',
                                    innerRadius: '100%',
                                    outerRadius: '105%'
                                }],
                            title: {
                                /* text: null*/
                                text: 'hrs'
                                /*
                                 style: {
                                 display: 'none'
                                 }*/
                            }
                        }],
                        plotOptions: {
                            gauge: {
                                dataLabels: {
                                    enabled: true
                                },
                                dial: {
                                    radius: '100%'
                                }
                            }
                        },
                        series: [{
                            data: [data]
                            /* yAxis: 0*/
                        }],
                        exporting: {
                            enabled: false
                        }
                    })
                });
            }






            /*$('#reload').on('click', function(){
             animate(*//*$('#inputVal').val()*//*4);
             });
             */
            $scope.dashboard=function(fData){
                var barColor = 'steelblue';
                function segColor(c){ return {high:"#41ab5d"}[c]; }

                // compute total for each state.
                fData.forEach(function(d){d.total=d.freq.high;});

                // function to handle histogram.
                function histoGram(fD){
                    var hG={},    hGDim = {t: 60, r: 0, b: 30, l: 0};
                    hGDim.w = 500 - hGDim.l - hGDim.r,
                        hGDim.h = 300 - hGDim.t - hGDim.b;
                    d3.select("body #dashboardhistogram svg").remove()
                    //create svg for histogram.
                    var hGsvg = d3.select("body #dashboardhistogram ").append("svg")
                        .attr("width", hGDim.w + hGDim.l + hGDim.r)
                        .attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
                        .attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");

                    // create function for x-axis mapping.
                    var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.1)
                        .domain(fD.map(function(d) { return d[0]; }));

                    // Add x-axis to the histogram svg.
                    hGsvg.append("g").attr("class", "x axis")
                        .attr("transform", "translate(0," + hGDim.h + ")")
                        .call(d3.svg.axis().scale(x).orient("bottom"));

                    // Create function for y-axis map.
                    var y = d3.scale.linear().range([hGDim.h, 0])
                        .domain([0, d3.max(fD, function(d) { return d[1]; })]);

                    // Create bars for histogram to contain rectangles and freq labels.
                    var bars = hGsvg.selectAll(".bar").data(fD).enter()
                        .append("g").attr("class", "bar");

                    //create the rectangles.
                    bars.append("rect")
                        .attr("x", function(d) { return x(d[0]); })
                        .attr("y", function(d) { return y(d[1]); })
                        .attr("width", x.rangeBand())
                        .attr("height", function(d) { return hGDim.h - y(d[1]); })
                        .attr('fill',barColor)
                        .on("mouseover",mouseover)// mouseover is defined below.
                        .on("mouseout",mouseout);// mouseout is defined below.

                    //Create the frequency labels above the rectangles.
                    bars.append("text").text(function(d){ return d3.format(",")(d[1])})
                        .attr("x", function(d) { return x(d[0])+x.rangeBand()/2; })
                        .attr("y", function(d) { return y(d[1])-5; })
                        .attr("text-anchor", "middle");

                    function mouseover(d){  // utility function to be called on mouseover.
                        // filter for selected state.
                        var st = fData.filter(function(s){ return s.State == d[0];})[0],
                            nD = d3.keys(st.freq).map(function(s){ return {type:s, freq:st.freq[s]};});
                        console.log(d)
                        var datahp1=$filter('number')(d[3] , 2)
                        var datahp2=$filter('number')(d[2] , 2)
                        $scope.chartOne1(datahp1);
                        $scope.chartOne(datahp2);
                        var datap1=$filter('number')(d[5], 2);
                        var datap2=$filter('number')(d[6] , 2);
                        var datap3=$filter('number')(d[9] , 2);
                        var datap4=$filter('number')(d[10] , 2);
                        console.log(datap1,datap2)
                        $scope.fiunctionone(datap1,datap2,datap3,datap4)
                        $scope.chartSndR(d[8]);
                        // animate(/*$('#inputVal').val()*/.1);
                        var smuda=parseInt($filter('number')(d[7] , 2))
                        $scope.renddata(smuda)
                        $scope.startvalue=10
                        var datapro=40
                        var datapro1=100
                        // $(".progress-bar").animate({width: datapro }, 100);
                        //$(".progone").animate({width: datapro1 }, 100);
                    }

                    function mouseout(d){    // utility function to be called on mouseout.
                        // reset the pie-chart and legend.
                        //  pC.update(tF);
                        // leg.update(tF);
                    }

                    // create function to update the bars. This will be used by pie-chart.
                    hG.update = function(nD, color){
                        // update the domain of the y-axis map to reflect change in frequencies.
                        y.domain([0, d3.max(nD, function(d) { return d[1]; })]);

                        // Attach the new data to the bars.
                        var bars = hGsvg.selectAll(".bar").data(nD);

                        // transition the height and color of rectangles.
                        bars.select("rect").transition().duration(500)
                            .attr("y", function(d) {return y(d[1]); })
                            .attr("height", function(d) { return hGDim.h - y(d[1]); })
                            .attr("fill", color);

                        // transition the frequency labels location and change value.
                        bars.select("text").transition().duration(500)
                            .text(function(d){ return d3.format(",")(d[1])})
                            .attr("y", function(d) {return y(d[1])-5; });
                    }
                    return hG;
                }





                // calculate total frequency by segment for all state.
                var tF = ['high'].map(function(d){
                    return {type:d, freq: d3.sum(fData.map(function(t){ return t.freq[d];}))};
                });

                // calculate total frequency by state for all segment.
                var sF = fData.map(function(d){return [d.State,d.total, d.PROD_ACTUAL_EXCAVATOR, d.PROD_TARGET_EXCAVATOR,
                    d.PROD_ACH_EXCAVATOR, d.UTL_ACTUAL_EXCAVATOR, d.UTL_TARGET_EXCAVATOR, d.SMU_EXCAVATOR,d.PROD_EXCAVATOR, d.UTL_ACH_EXCAVATOR, d.prod_ACH_EXCAVATOR];});

                var hG = histoGram(sF) // create the histogram.
                // create the pie-chart.
                // create the legend.




            }


            $scope.chartSndR=function(data){
                d3.select("body #fillgauge1 g").remove()
                var config5 = liquidFillGaugeDefaultSettings();
                config5.circleThickness = 0.1;
                config5.circleColor = "rgb(23, 139, 202)";
                config5.textColor = "#0E5144";
                config5.waveTextColor = "rgb(23, 139, 202)";
                config5.waveColor = "rgb(23, 139, 202)";
                config5.textVertPosition = 0.52;
                config5.waveAnimateTime = 5000;
                config5.waveHeight = 0;
                config5.waveAnimate = true;
                config5.waveCount = 2;
                config5.waveOffset = 0.15;
                config5.textSize = 0.5;
                config5.minValue = 0;
                config5.maxValue = 300000
                config5.displayPercent = false;
                var gauge6 = loadLiquidFillGauge("fillgauge1", data , config5);
            }




        });
    });
