'use strict';

angular.module('demoAppApp')
    .controller('kpi3Controller', function ($scope, Principal,$mdDialog,DataSource,$filter) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;


            $scope.startvalue=40
            $scope.sendvalue=80
            $scope.startdate=new Date()
            $scope.harvestingProvince = [
                "OB_PRODUCTION",
                "COAL_PRODUCTION",
                "STRIPPING_RATIO",
                "FUEL_CONSUMPTION",
                "FUEL_RATIO"

            ];
            $scope.chartname="OB_PRODUCTION"
            $scope.MTDdataMaster=[]
            $scope.changeData=function(id,date){

                DataSource.kpiThree(date).then(function (data) {

                    angular.copy(data,$scope.MTDdataMaster)
                    if($scope.chartname=='OB_PRODUCTION'){

                        $scope.label5="ACH"
                    }

                    if($scope.chartname=='COAL_PRODUCTION'){

                        $scope.label5="ACH"
                    }

                    if($scope.chartname=='STRIPPING_RATIO'){

                        $scope.label5="DEV(1-2)"
                    }
                    if($scope.chartname=='FUEL_CONSUMPTION'){

                        $scope.label5="DIFF((1-2)/2)"
                    }
                    if($scope.chartname=='FUEL_RATIO'){

                        $scope.label5="DIFF((1-2)/2)"
                    }

                    $scope.chartdataR=[
                        ['x'],
                        ['Actual'],
                        ['Daily Target'],
                        ['ACH'],
                        ['Target']
                    ]
                    $scope.HaulRdata=[]

                    $scope.chartAllData=[]
                    $scope.chartLastData=[
                        ['x'],
                        ['Actual'],
                        ['Target']
                    ]
                    $scope.chartdataRdisplay=[]
                    angular.forEach($scope.MTDdataMaster.rows,function(thisdatalist,key) {



                        if(thisdatalist[2]==id ){
                                       // console.log(thisdatalist)

                            $scope.HaulRdata.push([
                                'Day Shift',thisdatalist[1],thisdatalist[3],thisdatalist[3]
                            ])
                            $scope.HaulRdata.push([
                                'Night Shift',thisdatalist[1],thisdatalist[4],thisdatalist[4]
                            ])
                         //   console.log(thisdatalist[1],thisdatalist[11],thisdatalist[12])
                            $scope.chartdataR[0].push(thisdatalist[1])
                            $scope.chartdataR[1].push(thisdatalist[14])
                            $scope.chartdataR[2].push(thisdatalist[11])
                            $scope.chartdataR[3].push(thisdatalist[13])
                            $scope.chartdataR[4].push(thisdatalist[12])

                            $scope.chartAllData.push({
                                'project':thisdatalist[1],
                                'Actual':parseInt(thisdatalist[5]),
                                'Target':parseInt(thisdatalist[6]),
                                'ACH':parseInt(thisdatalist[7]),
                            })
                            console.log(typeof thisdatalist[5])
                            $scope.chartLastData[0].push(thisdatalist[1])
                            $scope.chartLastData[1].push(thisdatalist[8])
                            $scope.chartLastData[2].push(thisdatalist[9])
                            $scope.chartdataRdisplay.push(parseInt(thisdatalist[10]))
                        }




                    });


                    var sales_data=[
                        ['Day Shift','Project1',10,10],
                        ['Night Shift','Project2',10,10],
                        ['Day Shift','Project2',20,20],
                        ['Night Shift','Project3',20,20]


                    ];

                    console.log($scope.HaulRdata)
                    $scope.chartRenderOne($scope.HaulRdata)
                    if($scope.chartname=='STRIPPING_RATIO' || $scope.chartname=='FUEL_CONSUMPTION' || $scope.chartname=='FUEL_RATIO'){
                        $scope.chartdataR.splice(2,1);
                     //   delete $scope.chartdataR[2]
                        console.log($scope.chartdataR)
                        $scope.renderSplineChart1($scope.chartdataR)
                    }
                    if($scope.chartname=='OB_PRODUCTION' ||$scope.chartname=='COAL_PRODUCTION'){
                        console.log($scope.chartdataR)
                        $scope.renderSplineChart($scope.chartdataR)
                    }

                    /*   if($scope.chartname=='FUEL_CONSUMPTION'){

                           $scope.label5="DIFF((1-2)/2)"
                       }
                       if($scope.chartname=='FUEL_RATIO'){

                           $scope.label5="DIFF((1-2)/2)"
                       }*/


                    var full = [
                        ['x', 'Tier I', 'Tier II', 'Tier III'],
                        ['Actual', 6, 13, 4],
                        ['Target', 3, 3, 5]

                    ];
                    $scope.renderleft($scope.chartLastData);

                });

            }



































          /*  $(function() {
                $('input[name="daterange"]').daterangepicker();
            });*/

            $(function() {
                $('input[name="daterange"]').daterangepicker({
                        locale: {
                            format: 'DD-MM-YYYY'
                        },
                        singleDatePicker: true,
                        showDropdowns: true
                    },
                    function(start, end, label) {
                        var years = moment().diff(start, 'years');
                        alert("You are " + years + " years old.");
                    });
            });
            //.attr("transform","translate(0,0)")
            $scope.chartRenderOne=function(data){
                var width = 650, height = 680, margin ={b:0, t:40, l:170, r:0};
                d3.select("body #bipart svg").remove()
                var svg = d3.select("body #bipart")
                    .append("svg").attr('width',width).attr('height',(height+margin.b+margin.t))
                    .append("g").attr("transform","translate("+ margin.l+","+margin.t+")");

                var data = [
                    //{data:bP.partData(sales_data,4), id:'SalesAttempts', header:["Shift","Project", "Sales Attempts"]},
                    {data:bP.partData(data,3), id:'Sales', header:["Shift","Project", ""]}
                ];

                bP.draw(data, svg);


            }



            $scope.renderSplineChart=function(data){
                var chartothone = c3.generate({
                    bindto: '#splinemain',
                    data: {
                        x : 'x',
                        columns:data,

                        type: 'spline'
                    },
                    axis : {
                        x: {
                            type: 'category' // this needed to load string x value
                        }
                    }
                });
             //   d3.select("body #container1 .c3-target-data").remove()
            }
            $scope.renderSplineChart1=function(data){
                var chartothone = c3.generate({
                    bindto: '#splinemain',
                    data: {
                        x : 'x',
                        columns:data,

                        type: 'spline'
                    },
                    axis : {
                        x: {
                            type: 'category' // this needed to load string x value
                        }
                    }
                });
                //   d3.select("body #container1 .c3-target-data").remove()
            }


            $scope.renderRight=function(){
                var data = {
                    labels: [
                        'Project 1', 'Project 2', 'Project 3',
                        'Project 4', 'Project 5', 'Project 6',
                        'Project 7', 'Project 8', 'Project 9'
                    ],
                    series: [
                        {
                            label: 'Actual',
                            values: [1, 8, 15, 16, 23, 35,16, 23, 33 ]
                        },
                        {
                            label: 'Target',
                            values: [12, 3, 22, 11, 13, 25,12, 35, 22]
                        },
                        {
                            label: 'ACH',
                            values: [10, 3, 22, 11, 13, 25,12, 35, 22]
                        }
                        /*{
                         label: '2014',
                         values: [31, 28, 14, 8, 15, 21]
                         },*/
                    ]
                };

                var chartWidth       = 150,
                    barHeight        = 20,
                    groupHeight      = barHeight * data.series.length,
                    gapBetweenGroups = 10,
                    spaceForLabels   =70,
                    spaceForLegend   = 100;

                // Zip the series data together (first values, second values, etc.)
                var zippedData = [];
                for (var i=0; i<data.labels.length; i++) {
                    for (var j=0; j<data.series.length; j++) {
                        zippedData.push(data.series[j].values[i]);
                    }
                }

                // Color scale
                var color = d3.scale.category20();
                var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;

                var x = d3.scale.linear()
                    .domain([0, d3.max(zippedData)])
                    .range([0, chartWidth]);

                var y = d3.scale.linear()
                    .range([chartHeight + gapBetweenGroups, 0]);

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .tickFormat('')
                    .tickSize(0)
                    .orient("left");

                // Specify the chart area and dimensions
                var chart = d3.select(".chart")
                    .attr("width", spaceForLabels + chartWidth + spaceForLegend)
                    .attr("height", chartHeight);

                // Create bars
                var bar = chart.selectAll("g")
                    .data(zippedData)
                    .enter().append("g")
                    .attr("transform", function(d, i) {
                        return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i/data.series.length))) + ")";
                    });

                // Create rectangles of the correct width
                bar.append("rect")
                    .attr("fill", function(d,i) { return color(i % data.series.length); })
                    .attr("class", "bar")
                    .attr("width", x)
                    .attr("height", barHeight - 1);

                // Add text label in bar
                bar.append("text")
                    .attr("x", function(d) { return x(d) - 3; })
                    .attr("y", barHeight / 2)
                    .attr("fill", "red")
                    .attr("dy", ".35em")
                    .text(function(d,v) {
                        console.log(d)

                        return d;
                    });

                // Draw labels
                bar.append("text")
                    .attr("class", "label")
                    .attr("x", function(d) { return - 10; })
                    .attr("y", groupHeight / 2)
                    .attr("dy", ".35em")
                    .text(function(d,i) {
                        if (i % data.series.length === 0)
                            return data.labels[Math.floor(i/data.series.length)];
                        else
                            return ""});

                chart.append("g")
                    .attr("class", "y axis")
                    .attr("transform", "translate(" + spaceForLabels + ", " + -gapBetweenGroups/2 + ")")
                    .call(yAxis);

                // Draw legend
                var legendRectSize = 10,
                    legendSpacing  = 4;

               /* var legend = chart.selectAll('.legend')
                    .data(data.series)
                    .enter()
                    .append('g')
                    .attr('transform', function (d, i) {
                        var height = legendRectSize + legendSpacing;
                        var offset = -gapBetweenGroups/2;
                        var horz = spaceForLabels + chartWidth + 40 - legendRectSize;
                        var vert = i * height - offset;
                        return 'translate(' + horz + ',' + vert + ')';
                    });

                legend.append('rect')
                    .attr('width', legendRectSize)
                    .attr('height', legendRectSize)
                    .style('fill', function (d, i) { return color(i); })
                    .style('stroke', function (d, i) { return color(i); });

                legend.append('text')
                    .attr('class', 'legend')
                    .attr('x', legendRectSize + legendSpacing)
                    .attr('y', legendRectSize - legendSpacing)
                    .text(function (d) { return d.label; });*/
            }
            $scope.renderRight();

            $scope.renderleft=function(data){


                var chart = c3.generate({
                    data: {
                        x: 'x',
                        columns: data,
                        type: 'bar',
                    },
                    bar: {
                        width: {
                            ratio: 0.8 // this makes bar width 50% of length between ticks
                        }
                    },
                    axis: {
                        x: {
                            type: 'category' // this is needed to load string x value
                        }
                    },
                    bindto: '#bipartNext'
                });
            }

            var dataSelectother='2015-05-26'
            $scope.changeData( $scope.chartname,dataSelectother)
            $scope.label1="Daily Production(Bcm)"
            $scope.label2="Daily Production(Bcm)"
            $scope.label3="MTD Production(Bcm)"
            $scope.label4="Outlook Production(Bcm)"

            $scope.changeDateList=function(){

                if( typeof $scope.user !='undefined'){
                    var dataSelect=$filter('date')($scope.user.submissionDate, "yyyy-MM-dd");
                    console.log('fst')
                    console.log(dataSelect)
                    console.log($scope.chartname)
                    $scope.changeData($scope.chartname,dataSelect)
                }else{

                    console.log(dataSelectother)
                    console.log($scope.chartname)
                    $scope.changeData($scope.chartname,dataSelectother)
                }


                if($scope.chartname=='OB_PRODUCTION'){
                    $scope.label1="Daily Production(Bcm)"
                    $scope.label2="Daily Production(Bcm)"
                    $scope.label3="MTD Production(Bcm)"
                    $scope.label4="Outlook Production(Bcm)"
                    $scope.label5="ACH"
                }

                if($scope.chartname=='COAL_PRODUCTION'){
                    $scope.label1="Daily Production(Ton)"
                    $scope.label2="Daily Production(Ton)"
                    $scope.label3="MTD Production(Ton)"
                    $scope.label4="Outlook Production(Ton)"
                    $scope.label5="ACH"
                }

                if($scope.chartname=='STRIPPING_RATIO'){
                    $scope.label1="SR DAILY"
                    $scope.label2="SR DAILY"
                    $scope.label3="SR MTD"
                    $scope.label4="SR OUTLOOK"
                    $scope.label5="DEV(1-2)"
                }
                if($scope.chartname=='FUEL_CONSUMPTION'){
                    $scope.label1="DAILY FUEL CONSUMPTION(Ltr)"
                    $scope.label2="DAILY FUEL CONSUMPTION(Ltr)"
                    $scope.label3="MTD FUEL CONSUMPTION(Ltr)"
                    $scope.label4="OUTLOOK FUEL CONSUMPTION(Ltr)"
                    $scope.label5="DIFF((1-2)/2)"
                }
                if($scope.chartname=='FUEL_RATIO'){
                    $scope.label1="DAILY FUEL RATIO(Ltr/Bcm)"
                    $scope.label2="DAILY FUEL RATIO(Ltr/Bcm)"
                    $scope.label3="MTD FUEL RATIO(Ltr/Bcm)"
                    $scope.label4="OUTLOOK FUEL RATIO(Ltr/Bcm)"
                    $scope.label5="DIFF((1-2)/2)"
                }
            }
        });
    });
function popcontrollerlistother($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
