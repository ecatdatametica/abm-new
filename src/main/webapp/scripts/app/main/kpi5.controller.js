'use strict';
angular.module('demoAppApp')
    .controller('kpi5Controller', function ($scope, $filter,Principal,DataSource) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            var date='2014-12-01'+'/'+'2014-12-31'
            DataSource.kpiFive(date).then(function (data) {
                //   console.log(data);

                $scope.mainData = []
                angular.copy(data, $scope.mainData);

                $scope.dataMHUTJL1 = [];
                $scope.dataMHUTJL2 = [];
                $scope.dataMSJ    = [];
                $scope.dataRBH    = [];
                $scope.dataREVTTN = [];
                $scope.dataRJM    = [];
                $scope.dataRJN    = [];
                $scope.dataTMJ    = [];
                $scope.dataTTN    = [];

                angular.forEach($scope.mainData.rows,function(thisdata){

                    var d      = new Date(thisdata[0]);
                    var dateVal= d.getDate();

                    switch (thisdata[1]) {
                        case "MHU-TJL":

                            $scope.dataMHUTJL1.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL1.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL1.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL1.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL1.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;

                        case "MHUTJL":

                            $scope.dataMHUTJL2.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL2.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL2.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL2.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataMHUTJL2.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;

                        case "MSJ":
                            $scope.dataMSJ.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataMSJ.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataMSJ.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataMSJ.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataMSJ.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;

                        case "RBH":
                            $scope.dataRBH.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataRBH.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataRBH.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataRBH.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataRBH.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;

                        case "REVTTN":
                            $scope.dataREVTTN.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataREVTTN.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataREVTTN.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataREVTTN.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataREVTTN.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;

                        case "RJM":
                            $scope.dataRJM.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataRJM.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataRJM.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataRJM.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataRJM.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;

                        case "RJN":
                            $scope.dataRJN.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataRJN.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataRJN.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataRJN.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataRJN.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;


                        case "TMJ":
                            $scope.dataTMJ.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataTMJ.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataTMJ.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataTMJ.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataTMJ.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;


                        case "TTN":
                            $scope.dataTTN.push({
                                x_Coordinate: 'REV_OB_FINAL',
                                y_Coordinate: thisdata[2],
                                dateVal: dateVal
                            });
                            $scope.dataTTN.push({
                                x_Coordinate: 'REV_CG_FINAL',
                                y_Coordinate: thisdata[3],
                                dateVal: dateVal
                            });
                            $scope.dataTTN.push({
                                x_Coordinate: 'REV_CH_FINAL',
                                y_Coordinate: thisdata[4],
                                dateVal: dateVal
                            });
                            $scope.dataTTN.push({
                                x_Coordinate: 'REV_RM_FINAL',
                                y_Coordinate: thisdata[5],
                                dateVal: dateVal
                            });
                            $scope.dataTTN.push({
                                x_Coordinate: 'REV_RENTAl_FINAL',
                                y_Coordinate: thisdata[6],
                                dateVal: dateVal
                            });
                            break;
                    }



                    });


                $scope.date1=[];

                angular.forEach($scope.dataMHUTJL2,function(thisdata) {

                    if(thisdata.dateVal==1) {
                        $scope.date1.push({
                            x_Coordinate: thisdata.x_Coordinate,
                            y_Coordinate: thisdata.y_Coordinate,
                            dateVal: thisdata.dateVal
                        });
                    }
                });

                    $scope.createGraph($scope.date1);
                $scope.loadPoints($scope.date1);

            });



                 var g, x, y,xAxis,yAxis,chart,main,tip;
            $scope.createGraph =function(data){

                var margin = {top: 35, right: 15, bottom: 60, left: 80}
                    , width = 800 - margin.left - margin.right
                    , height = 590 - margin.top - margin.bottom;

                x = d3.scale.ordinal()
                    .rangeRoundBands([0, width], 1);

                x.domain(
                    data.map(function (d) {
                        return d.x_Coordinate
                    })
                );

                 y = d3.scale.linear()
                    .domain([0, d3.max(data, function (d) {
                        return d.y_Coordinate
                    })])
                    .range([height, 0]);

                 chart = d3.select('body #scatter1')
                    .append('svg:svg')
                    .attr('width', width + margin.right + margin.left)
                    .attr('height', height + margin.top + margin.bottom)
                    .attr('class', 'chart')


                main = chart.append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
                    .attr('width', width)
                    .attr('height', height)
                    .attr('class', 'main')

                // draw the x axis
                xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

                main.append('g')
                    .attr('transform', 'translate(0,' + height + ')')
                    .attr('class', 'x axis')
                    .call(xAxis);

                // draw the y axis
                yAxis = d3.svg.axis()
                    .scale(y)
                    .orient('left');

                main.append('g')
                    .attr('transform', 'translate(0,0)')
                    .attr('class', 'y axis')
                    .call(yAxis);

                 g = main.append("svg:g")
            };
            var arr=[];



            $scope.loadPoints = function(data){
                g.selectAll("scatter-dots")
                    .data(data)
                    .enter().append("svg:circle")
                    .attr("cx", function (d, i) {
                        return x(d.x_Coordinate)
                    })
                    .attr("cy", function (d) {
                        return y(d.y_Coordinate);
                    })
                    .attr("r", 3)


            }



            var arr=[];
            $scope.LoadProjectData = function (pName) {
                switch(pName){
                    case "MHU-TJL":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataMHUTJL1);
                        break;

                    }

                    case "MHUTJL":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataMHUTJL2);
                        break;
                    }

                    case "MSJ":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataMSJ);
                        break;

                    }

                    case "RBH":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataRBH);
                        break;

                    }

                    case "REVTTN":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataREVTTN);
                        break;

                    }

                    case "RJM":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataRJM);
                        break;

                    }

                    case "RJN":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataRJN);
                        break;

                    }

                    case "TMJ":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataTMJ);
                        console.log($scope.dataTMJ);

                        break;

                    }

                    case "TTN":{
                        arr=[];
                        d3.select("body #scatter1 svg").remove();
                        $scope.dataforDateOne($scope.dataTTN);
                        break;

                    }

                }

            }



            $scope.dataforDateOne = function(data){

                if(data.length == 0){
                    console.log('not avaialable');
                    var datax=[{'x_Coordinate': 'REV_OB_FINAL','y_Coordinate': 2,'DATE_VAL'   : 1},
                        {'x_Coordinate': 'REV_CG_FINAL','y_Coordinate': 1000,'DATE_VAL'   : 2},
                        {'x_Coordinate': 'REV_CH_FINAL','y_Coordinate': 500,'DATE_VAL'   : 3},
                        {'x_Coordinate': 'REV_RM_FINAL','y_Coordinate': 200,'DATE_VAL'   : 4},
                        {'x_Coordinate': 'REV_RENTAL_FINAL','y_Coordinate': 200,'DATE_VAL'   : 5}
                    ]
                    $scope.createGraph(datax);
                    //$scope.loadPoints($scope.date1);
                }
                else {
                    $scope.date1 = [];

                    angular.forEach(data, function (thisdata) {

                        if (thisdata.dateVal == 1) {
                            $scope.date1.push({
                                x_Coordinate: thisdata.x_Coordinate,
                                y_Coordinate: thisdata.y_Coordinate,
                                dateVal: thisdata.dateVal
                            });
                        }
                    });

                    $scope.createGraph($scope.date1);
                    $scope.loadPoints($scope.date1);
                }
            }



            $scope.checkData = function (excludeVal,counttimes,projectSelected) {


                var dataSetName='$scope.dataMHUTJL2';


                arr.push(excludeVal);
                console.log(arr);

                switch(projectSelected){
                    case "dataMHUTJL1":{
                        dataSetName=$scope.dataMHUTJL1;
                        break;
                    }

                    case "dataMHUTJL2":{
                        dataSetName=$scope.dataMHUTJL2;
                        break;
                    }

                    case "dataMSJ":{
                        dataSetName=$scope.dataMSJ;
                        break;

                    }

                    case "dataRBH":{
                        dataSetName=$scope.dataRBH;
                        break;

                    }

                    case "dataREVTTN":{
                        dataSetName=$scope.dataREVTTN;
                        break;

                    }

                    case "dataRJM":{
                        dataSetName=$scope.dataRJM;
                        break;

                    }

                    case "dataRJN":{
                        dataSetName=$scope.dataRJN;
                        break;

                    }

                    case "dataTMJ":{
                        dataSetName=$scope.dataTMJ;
                        break;
                    }

                    case "dataTTN":{
                        dataSetName=$scope.dataTTN;
                        break;

                    }
                }

                $scope.date1=[];

                var i= 0,xData;
                for(i=0;i<arr.length;i++) {
                    xData = arr[i];
                    angular.forEach(dataSetName, function (thisdata) {

                        if (thisdata.dateVal == xData) {
                            $scope.date1.push({
                                x_Coordinate: thisdata.x_Coordinate,
                                y_Coordinate: thisdata.y_Coordinate,
                                dateVal: thisdata.dateVal
                            });
                            console.log(dataSetName.y_Coordinate, excludeVal);
                        }


                    });
                }

                d3.select("body #scatter1 svg").remove();
                if($scope.date1.length == 0){
                    var datax=[{'x_Coordinate': 'REV_OB_FINAL','y_Coordinate': 2,'DATE_VAL'   : 1},
                        {'x_Coordinate': 'REV_CG_FINAL','y_Coordinate': 70000,'DATE_VAL'   : 2},
                        {'x_Coordinate': 'REV_CH_FINAL','y_Coordinate': 500,'DATE_VAL'   : 3},
                        {'x_Coordinate': 'REV_RM_FINAL','y_Coordinate': 200,'DATE_VAL'   : 4},
                        {'x_Coordinate': 'REV_RENTAL_FINAL','y_Coordinate': 200,'DATE_VAL'   : 5}
                    ]
                    $scope.createGraph(datax);
                    //$scope.loadPoints($scope.date1);
                }
                else {
                    $scope.createGraph($scope.date1);
                    $scope.loadPoints($scope.date1);
                }

                if((counttimes%2)==0){
                    //even means checked, show data
                    console.log("clicked even");
/*
                    angular.forEach(dataSetName,function(thisdata){

                        if(thisdata.dateVal == excludeVal){
                            console.log(thisdata);
                            findata.push(thisdata);
                        }
                    })
                    d3.select("body #scatter1 svg").remove();

                    $scope.createGraph(findata);
                    $scope.loadPoints(findata);*/
                }
                else{
                    console.log("clicked odd");
                    //odd means unchecked, hide data
                    //angular.forEach(dataSetName,function(thisdata){
                    //
                    //    if(thisdata.dateVal != excludeVal){
                    //        console.log(thisdata)
                    //        findata.push(thisdata)
                    //    }
                    //});
                    //
                    //d3.select("body #scatter1 svg").remove();
                    //$scope.createGraph(findata);
                    //$scope.loadPoints(findata);

                }

              }



        });
    });