/* globals $ */
'use strict';

angular.module('demoAppApp')
    .directive('demoAppAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
