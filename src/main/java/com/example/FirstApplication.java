package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class FirstApplication {

	private static final Logger log = LoggerFactory.getLogger(FirstApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(FirstApplication.class, args);
        
    }
   /* @Override
    public void run(String... strings) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
 

        String quote = restTemplate.getForObject("http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date,bu,province,distrik&metrics=ACT_WOOD_PROD_MTD_CU,TRGT_WOOD_PROD_MTD_CU,ACT_WOOD_PROD_YTD_CU,TRGT_WOOD_PROD_YTD_CU,ACT_PLANT_MTD_CU,TRGT_PLANT_MTD_CU,ACT_PLANT_YTD_CU,TRGT_PLANT_YTD_CU,ACT_NURSERY_MTD_CU,TRGT_NURSARY_MTD_CU,ACT_NURSERY_YTD_CU,TRGT_NURSARY_YTD_CU,ACT_WOOD_PROD_MTD_PR,TRGT_WOOD_PROD_MTD_PR,ACT_WOOD_PROD_YTD_PR,TRGT_WOOD_PROD_YTD_PR,ACT_PLANT_MTD_PR,TRGT_PLANT_MTD_PR,ACT_PLANT_YTD_PR,TRGT_PLANT_YTD_PR,ACT_NURSERY_MTD_PR,TRGT_NURSARY_MTD_PR,ACT_NURSERY_YTD_PR,TRGT_NURSARY_YTD_PR&fromDate=2015-09-29&toDate=2015-09-29", String.class);
        log.info(quote.toString());
    }*/
}
