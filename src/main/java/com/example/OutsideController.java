package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class OutsideController {

   

    @RequestMapping("/firstdata")
    public @ResponseBody String greeting() {
    	RestTemplate restTemplate = new RestTemplate();
    	 

        String quote = restTemplate.getForObject("http://10.30.160.201:8080/data-service/clickstream/data?dimensions=load_date,bu,province,distrik&metrics=ACT_WOOD_PROD_MTD_CU,TRGT_WOOD_PROD_MTD_CU,ACT_WOOD_PROD_YTD_CU,TRGT_WOOD_PROD_YTD_CU,ACT_PLANT_MTD_CU,TRGT_PLANT_MTD_CU,ACT_PLANT_YTD_CU,TRGT_PLANT_YTD_CU,ACT_NURSERY_MTD_CU,TRGT_NURSARY_MTD_CU,ACT_NURSERY_YTD_CU,TRGT_NURSARY_YTD_CU,ACT_WOOD_PROD_MTD_PR,TRGT_WOOD_PROD_MTD_PR,ACT_WOOD_PROD_YTD_PR,TRGT_WOOD_PROD_YTD_PR,ACT_PLANT_MTD_PR,TRGT_PLANT_MTD_PR,ACT_PLANT_YTD_PR,TRGT_PLANT_YTD_PR,ACT_NURSERY_MTD_PR,TRGT_NURSARY_MTD_PR,ACT_NURSERY_YTD_PR,TRGT_NURSARY_YTD_PR&fromDate=2015-09-29&toDate=2015-09-29", String.class);
        return quote;
    }


     @RequestMapping("/kpiOne/{date}")
        public @ResponseBody String kpiOne(@PathVariable String date) {
        	RestTemplate restTemplate = new RestTemplate();

            String url = "http://10.200.99.107:8080/data-service/coverageAnalytics/data?dimensions=PROD_DATE&metrics=PROJECT_NM,SERVICE_ID,MACHINE_NM_HAULER,MACHINE_NM_EXCAVATOR,DISTANCE_HAULER,DISTANCE_EXCAVATOR,SMU_HAULER,SMU_EXCAVATOR,PROD_HAULER,PROD_EXCAVATOR,PROD_ACTUAL_HAULER,PROD_ACTUAL_EXCAVATOR,PROD_TARGET_HAULER,PROD_TARGET_EXCAVATOR,PROD_ACH_HAULER,PROD_ACH_EXCAVATOR,QTY_ACTUAL_HAULER,QTY_ACTUAL_EXCAVATOR,QTY_TARGET_HAULER,QTY_TARGET_EXCAVATOR,UTL_ACTUAL_HAULER,UTL_ACTUAL_EXCAVATOR,UTL_TARGET_HAULER,UTL_TARGET_EXCAVATOR,UTL_ACH_HAULER,UTL_ACH_EXCAVATOR&fromDate="+date+"&toDate="+date;
            String quote = restTemplate.getForObject(url, String.class);
            return quote;
        }
     @RequestMapping("/kpiThree/{date}")
        public @ResponseBody String kpiThree(@PathVariable String date) {
        	RestTemplate restTemplate = new RestTemplate();

            String url = "http://10.200.99.107:8080/data-service/coverageAnalytics/data?dimensions=PROD_DATE_1&metrics=PROJECT_NAME,SERVICE_ID,DAILY_PROD_DS,DAILY_PROD_NS,DAILY_PROD_TOTAL,DAILY_PROD_TARGET,DAILY_PROD_ACH,MTD_PROD_TOTAL,MTD_PROD_TARGET,MTD_PROD_ACH,OUTLOOK_PROD_TOTAL,OUTLOOK_PROD_TARGET,OUTLOOK_PROD_ACH,DAILY_TARGET&fromDate="+date+"&toDate="+date;
            String quote = restTemplate.getForObject(url, String.class);
            return quote;
        }
            @RequestMapping("/kpiTwo/{date}")
                             public @ResponseBody String kpiTwo(@PathVariable String date) {
                             	RestTemplate restTemplate = new RestTemplate();

                                 String url = "http://10.200.99.107:8080/data-service/coverageAnalytics/data?dimensions=PROD_DATE&metrics=PROJECT_NM,SERVICE_ID,MACHINE_NM_HAULER,MACHINE_NM_EXCAVATOR,DISTANCE_HAULER,DISTANCE_EXCAVATOR,SMU_HAULER,SMU_EXCAVATOR,PROD_HAULER,PROD_EXCAVATOR,PROD_ACTUAL_HAULER,PROD_ACTUAL_EXCAVATOR,PROD_TARGET_HAULER,PROD_TARGET_EXCAVATOR,PROD_ACH_HAULER,PROD_ACH_EXCAVATOR,QTY_ACTUAL_HAULER,QTY_ACTUAL_EXCAVATOR,QTY_TARGET_HAULER,QTY_TARGET_EXCAVATOR,UTL_ACTUAL_HAULER,UTL_ACTUAL_EXCAVATOR,UTL_TARGET_HAULER,UTL_TARGET_EXCAVATOR,UTL_ACH_HAULER,UTL_ACH_EXCAVATOR&fromDate="+date+"&toDate="+date;
                                 String quote = restTemplate.getForObject(url, String.class);
                                 return quote;
                }
       @RequestMapping("/kpiFour/{date}")
          public @ResponseBody String kpiFour(@PathVariable String date) {
          	RestTemplate restTemplate = new RestTemplate();

              String url = "http://10.200.99.107:8080/data-service/coverageAnalytics/data?dimensions=PROD_DATE_2&metrics=SALES_FACTOR,ACTUAL_PREV_COST,ACTUAL_COST,TARGET_COST,CVR_ACT,CVR_RFC,CVR_DEV,MDT_ACTUAL,MDT_TARGET,MDT_CVR_ACT,MDT_CVR_RFC,MDT_CVR_ACTUAL,MDT_COST_BCM,TARGET_COST_BCM&fromDate="+date+"&toDate="+date;
              String quote = restTemplate.getForObject(url, String.class);
              return quote;
          }

         @RequestMapping("/kpiFive/{date1}/{date2}")
                 public @ResponseBody String kpiFive(@PathVariable String date1,@PathVariable String date2) {
                 	RestTemplate restTemplate = new RestTemplate();

                     String url = "http://10.200.99.107:8080/data-service/coverageAnalytics/data?dimensions=PROD_DATE_3&metrics=PROJECT_NAME,REV_OB_FINAL,REV_CG_FINAL,REV_CH_FINAL,REV_RM_FINAL,REV_RENTAL_FINAL&fromDate="+date1+"&toDate="+date2;
                     String quote = restTemplate.getForObject(url, String.class);
                     return quote;
                 }

        @RequestMapping("/harvesting/{date}")
                public @ResponseBody String dateBasedHarvesting(@PathVariable String date) {
                	RestTemplate restTemplate = new RestTemplate();

                    String url = "http://10.30.160.201:8080/data-service/clickstream/data?dimensions=load_date_1,bu,province,distrik&metrics=PETAK_ID,HARVESTING_COMPLETED,START_DATE,END_DATE,ACT_WOOD_POTENCY,TRGT_WOOD_POTENCY,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK&fromDate="+date+"&toDate="+date;
                    String quote = restTemplate.getForObject(url, String.class);
                    return quote;
                }

                @RequestMapping("/plantation/{date}")
                         public @ResponseBody String dateBasedPlantation(@PathVariable String date) {
                         	RestTemplate restTemplate = new RestTemplate();

                             String url = "http://10.30.160.201:8080/data-service/clickstream/data?dimensions=load_date_2,bu,province,distrik,PLOT_ID,ROTATION&metrics=PLANTATION_COMPLETED,PLANTATION_START_DATE,PLANTATION_END_DATE,MAINTENANCE_START_DATE,MAINTENANCE_END_DATE,NURSERY_ORIGIN,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK&filters=ROTATION EQ 4&fromDate="+date+"&toDate="+date;
                             String quote = restTemplate.getForObject(url, String.class);
                             return quote;
                         }

                            @RequestMapping("/plantationHistory/{date}/{id}")
                                                  public @ResponseBody String dateBasedPlantationHistory(@PathVariable String date,@PathVariable String id) {
                                                  	RestTemplate restTemplate = new RestTemplate();

                                                      String url = "http://10.30.160.201:8080/data-service/clickstream/data?dimensions=load_date_2,bu,province,distrik,PLOT_ID,ROTATION&metrics=PLANTATION_COMPLETED,PLANTATION_START_DATE,PLANTATION_END_DATE,MAINTENANCE_START_DATE,MAINTENANCE_END_DATE,NURSERY_ORIGIN,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK&filters=ROTATION EQ 3,PLOT_ID EQ "+id+"&fromDate="+date+"&toDate="+date;
                                                      String quote = restTemplate.getForObject(url, String.class);
                                                      return quote;
                                                  }

    @RequestMapping("/snddata/{date}")
    public @ResponseBody String dateBased(@PathVariable String date) {
    	RestTemplate restTemplate = new RestTemplate();
    	 
        String url = "http://10.30.160.201:8080/data-service/clickstream/data?dimensions=load_date,bu,province,distrik&metrics=ACT_WOOD_PROD_MTD_CU,TRGT_WOOD_PROD_MTD_CU,ACT_WOOD_PROD_YTD_CU,TRGT_WOOD_PROD_YTD_CU,ACT_PLANT_MTD_CU,TRGT_PLANT_MTD_CU,ACT_PLANT_YTD_CU,TRGT_PLANT_YTD_CU,ACT_NURSERY_MTD_CU,TRGT_NURSARY_MTD_CU,ACT_NURSERY_YTD_CU,TRGT_NURSARY_YTD_CU,ACT_WOOD_PROD_MTD_PR,TRGT_WOOD_PROD_MTD_PR,ACT_WOOD_PROD_YTD_PR,TRGT_WOOD_PROD_YTD_PR,ACT_PLANT_MTD_PR,TRGT_PLANT_MTD_PR,ACT_PLANT_YTD_PR,TRGT_PLANT_YTD_PR,ACT_NURSERY_MTD_PR,TRGT_NURSARY_MTD_PR,ACT_NURSERY_YTD_PR,TRGT_NURSARY_YTD_PR&fromDate="+date+"&toDate="+date;
        String quote = restTemplate.getForObject(url, String.class);
        return quote;
    }

}